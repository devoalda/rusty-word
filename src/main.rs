use std::{io};
use crate::db::DB;

mod menu;
mod password_manager;
mod password;
mod db;
mod config;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    DB::user_table_creation().expect("Could not create user table!");

    println!("Welcome to Rusty Word!\nThis is a password manager written in Rust.");
    let mut password_manager: password_manager::PasswordManager;
    loop{
        // Loop until the user is authenticated
        // If the user enters 0 or nothing then exit the program
        // Read the master password from the user
        // password_manager = {
        //     password_manager::PasswordManager::new(0, String::from(""), &String::from(""))
        // };
        println!("Please enter your user name (or 0 to end): ");
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read line");
        let user_name = {
            if input.trim().is_empty() {
                println!("Received empty input, Exiting...");
                return Ok(());
            } else if input.trim() == "0" {
                println!("Exiting...");
                return Ok(());
            }
            // Remove the newline character from the master password
            input.trim().to_string()
        };
        println!("Please enter your master password (or 0 to end): ");
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read line");
        let master_password = {
            if input.trim().is_empty() {
                println!("Received empty input, Exiting...");
                return Ok(());
            } else if input.trim() == "0" {
                println!("Exiting...");
                return Ok(());
            }
            // Remove the newline character from the master password
            input.trim().to_string()
        };

        password_manager = {
            password_manager::PasswordManager::new(0, user_name, &master_password)
        };

        println!("Authenticating...");

        if password_manager.authenticate() {
            println!("Authentication successful!");
            break;
        } else {
            println!("Authentication failed! Please try again.");
        }
    }

    loop {
        // Main menu
        // 1. Add a password
        // 2. Remove a password
        // 3. Get a password
        // 4. Get all passwords
        // 5. Generate a password
        // 6. Logout

        println!("Enter 1 to add a password\n\
        Enter 2 to remove a password\n\
        Enter 3 to get a password\n\
        Enter 4 to get all passwords\n\
        Enter 5 to generate a password\n\
        Enter 6 to logout\n\
        Enter your choice: \
        ");

        let mut choice = String::new();
        io::stdin().read_line(&mut choice).expect("Failed to read line");
        let num_choice:u32 = {
            match choice.trim().parse() {
                Ok(num) => num,
                Err(_) => {
                    println!("Invalid choice!");
                    continue;
                }
            }
        };

        match num_choice {
            1 => {
                menu::add_password(&mut password_manager);
            },
            2 =>{
                menu::remove_password(&mut password_manager);
            },
            3 => {
                menu::get_password(&mut password_manager);
            },
            4 => {
                menu::get_all_password(&password_manager);
            },
            5 => {
                menu::gen_password(&mut password_manager);
            },
            6 => {
                password_manager.logout();
                break;
            },
            _ => println!("Invalid choice!"),
        };
    }
    Ok(())
}
