use std::{io, fs::File, io::prelude::*};
const CONFIG_PATH: &'static str = "./Resource/config.toml";

pub(crate) struct Config {
    db_path: String,
}

impl Config {
    pub fn new() -> Config {
        let mut config = Config {
            db_path: String::from(""),
        };
        let mut file = File::open(CONFIG_PATH).expect("Could not open config file!");
        let mut contents = String::new();
        file.read_to_string(&mut contents).expect("Could not read config file!");
        let config_toml: toml::Value = toml::from_str(&contents).expect("Could not parse config file!");
        config.db_path = config_toml["Database"]["path"].as_str().unwrap().to_string();
        config
    }

    pub fn get_db_path() -> String {
        let config = Config::new();
        config.db_path
    }
}