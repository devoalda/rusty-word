use std::fmt::Display;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
/// A struct to store the password information
pub struct Password {
    id: usize,
    name: String,
    username: String,
    password: String,
    url: String,
    notes: String,
}
impl Password {
    pub fn new(id: usize, name: String, username: String, password: String, url: String, notes: String) -> Password {
        //! Create a new Password
        //! # Arguments
        //! * `name` - The name of the password (String)
        //! * `username` - The username of the password (String)
        //! * `password` - The password (String)
        //! * `url` - The url of the password (String)
        //! * `notes` - The notes of the password (String)
        //! # Returns
        //! * `Password` - The Password struct
        Password {
            id,
            name,
            username,
            password,
            url,
            notes,
        }
    }

    pub fn get_name(&self) -> &String {
        //! Get the name of the password
        //! # Returns
        //! * `String` - The name of the password
        &self.name
    }

    pub fn get_username(&self) -> &String {
        //! Get the username of the password
        //! # Returns
        //! * `String` - The username of the password
        &self.username
    }

    pub fn get_password(&self) -> &String {
        //! Get the password
        //! # Returns
        //! * `String` - The password
        &self.password
    }

    pub fn get_url(&self) -> &String {
        //! Get the url of the password
        //! # Returns
        //! * `String` - The url of the password
        &self.url
    }

    pub fn get_notes(&self) -> &String {
        //! Get the notes of the password
        //! # Returns
        //! * `String` - The notes of the password
        &self.notes
    }

    pub fn get_id(&self) -> usize {
        //! Get the id of the password
        //! # Returns
        //! * `usize` - The id of the password
        self.id
    }

    // Setter functions (not used in the program, but can be used in the future)
    #[allow(dead_code)]
    pub fn set_name(&mut self, name: String) {
        //! Set the name of the password
        //! # Arguments
        //! * `name` - The name of the password (String)
        self.name = name;
    }

    #[allow(dead_code)]
    pub fn set_username(&mut self, username: String) {
        //! Set the username of the password
        //! # Arguments
        //! * `username` - The username of the password (String)
        self.username = username;
    }

    #[allow(dead_code)]
    pub fn set_password(&mut self, password: String) {
        //! Set the password
        //! # Arguments
        //! * `password` - The password (String)
        self.password = password;
    }

    #[allow(dead_code)]
    pub fn set_url(&mut self, url: String) {
        //! Set the url of the password
        //! # Arguments
        //! * `url` - The url of the password (String)
        self.url = url;
    }

    #[allow(dead_code)]
    pub fn set_notes(&mut self, notes: String) {
        //! Set the notes of the password
        //! # Arguments
        //! * `notes` - The notes of the password (String)
        self.notes = notes;
    }
}

impl Display for Password{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[[password.{}]]\nUsername = {}\nPassword = {}\nUrl = {}\nNotes = {}", self.name, self.username, self.password, self.url, self.notes)
    }
}
