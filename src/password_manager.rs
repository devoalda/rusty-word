#![allow(unused)]
use std::{env,
          fs::File,
          io::{Read, Write, Error, ErrorKind},
          fs,
          thread};
use std::fmt::format;
use std::sync::mpsc;
use serde::{Serialize, Deserialize, Deserializer};
use toml::{Table, map};
use crate::password::Password;
use rand_core::{OsRng, RngCore};
use argon2::{
    password_hash::{
        PasswordHash, PasswordHasher, PasswordVerifier, SaltString,
    },
    Argon2
};
use encryptfile as ef;
use rand::distributions::Alphanumeric;
use rand::Rng;
use rusqlite::Connection;
use crate::password;
// use crate::db;
use crate::db::DB;

/// A struct to store the passwords and the master password
#[derive(Debug, Clone)]
pub struct PasswordManager {
    user_id: usize,
    user_name: String,
    master_password: String,
    salt: String,
}

/// Password Manager implementation
impl PasswordManager {
    // const DB_PATH: &'static str = "./Resource/password_manager.db";
    pub fn new(user_id: usize, user_name: String, master_password: &String) -> PasswordManager {
        //! Create a new PasswordManager
        //! # Arguments
        //! * `user_name` - The user name of the user (String)
        //! * `master_password` - The master password of the user (&String)
        //! # Returns
        //! * `PasswordManager` - The PasswordManager struct
        PasswordManager {
            user_id,
            user_name,
            master_password: master_password.to_string(),
            salt: String::new(),
        }
    }

    pub fn add_password(&mut self, name: String, username: String, password: String, url: String, notes: String) {
        //! Add a new password to the PasswordManager
        //! # Arguments
        //! * `name` - The name of the password (String)
        //! * `username` - The username of the password (String)
        //! * `password` - The password (String)
        //! * `url` - The url of the password (String)
        //! * `notes` - The notes of the password (String)

        // Create a new password
        let mut password_id = match DB::get_max_password_id(self) {
        Ok(max_id) => max_id,
        Err(err) => {
            println!("Error getting max password ID: {:?}", err);
            return;
        }
        };
        let password = Password::new(password_id, name, username, password, url, notes);
        if let Err(err) = DB::add_password(password, self) {
            println!("Failed to add password to database: {:?}", err);
        }
    }
    pub fn remove_password_from_index (&mut self, index: usize) {
        //! Remove a password from the PasswordManager
        //! # Arguments
        //! * `index` - The index of the password (usize)

        // Remove the password from the database
        DB::remove_password(self, index);
    }

    pub fn get_password(self, name: &String) -> Result<Password, ()> {
        //! Get a password from the PasswordManager
        //! # Arguments
        //! * `name` - The name of the password (String)
        //! # Returns
        //! * `Password` - The password object

        // Get all passwords and then search for the password
        let passwords = self.get_passwords().unwrap();
        for password in passwords {
            if password.get_name().eq(name) {
                return Ok(password);
            }
        }
        Err(())
    }

    pub fn get_passwords(&self) -> Result<Vec<Password>, ()> {
        //! Get all the passwords from the PasswordManager
        //! # Returns
        //! * `Vec<Password>` - The vector of passwords
        let mut passwords:Vec<Password> = Vec::new();

        passwords = {
            match DB::get_all_password(self){
                Ok(passwords) => passwords,
                Err(_) => Vec::new()
            }
        };
        Ok(passwords)
    }

    pub fn get_user_name(&self) -> String {
        //! Get the user name of the user
        //! # Returns
        //! * `String` - The user name of the user
        self.user_name.to_string()
    }

    pub fn get_user_id(&self) -> usize {
        //! Get the user id of the user
        //! # Returns
        //! * `usize` - The user id of the user
        self.user_id
    }

    pub fn get_master_password(&self) -> String {
        //! Get the master password of the user
        //! # Returns
        //! * `String` - The master password of the user
        self.master_password.to_string()
    }

    pub fn get_salt(&self) -> String {
        //! Get the salt of the user
        //! # Returns
        //! * `String` - The salt of the user
        self.salt.to_string()
    }

    pub fn authenticate(&mut self) -> bool {
        //! Authenticate the user
        //! # Returns
        //! * `bool` - Whether the user was authenticated or not
        //! # Notes
        //! * If the user is new then the user's password will be hashed and stored in the file
        //! * If the user is existing then the user's password will be hashed and compared with the stored hash
        //! * If the user is authenticated then the passwords will be read from the file and populated in the vector
        //! * If the user is not authenticated then the passwords vector will be empty

        let argon2 = Argon2::default();
        let user_name = self.user_name.to_string();
        let master_password = self.master_password.to_string();
        self.salt = DB::get_salt(&user_name).expect("Unable to get salt");
        println!("Salt: {}", self.salt);
        let mut salt_string: SaltString;
        if self.salt.is_empty() {
            salt_string = SaltString::generate(&mut OsRng);
        } else{
            // convert salt to SaltString
            salt_string = SaltString::from_b64(&self.salt).expect("Unable to convert salt to SaltString");
        }

        self.salt = salt_string.to_string();
        // Hash the password
        self.master_password = argon2.hash_password(master_password.as_bytes(), &salt_string)
            .expect("Unable to hash password")
            .to_string();
        let auth:bool = DB::auth_user(self).expect("Unable to authenticate user");
        match auth {
            true => {
                println!("User authenticated successfully!");
                self.user_id = {
                    match DB::get_user_id(self) {
                        Ok(id) => id,
                        Err(_) => 0
                    }
                };
                DB::pass_table_creation(&self.user_name).expect("Unable to create password table");
                // self.passwords = DB::populate_user_password(self).expect("Unable to populate passwords");
                return true;
            }
            false => {
                // Create new user
                println!("User not found! Creating new user...");
                self.user_id = {
                    match DB::get_max_user_id() {
                        Ok(id) => id,
                        Err(_) => 0
                    }
                };

                // self.user_id = if self.user_id == 0 {0} else {self.user_id};
                DB::create_user(self).expect("Unable to create user");
                DB::pass_table_creation(&self.user_name).expect("Unable to create password table");
                return true;
            }
        }
    }

    pub fn logout(&mut self) {
        //! User Logout Function
        // Write the passwords to the file
        // let mut passwords_file_content = String::new();
        // for password in &self.passwords {
        //     passwords_file_content.push_str(&format!("{},{},{},{},{}\n", password.get_name(), password.get_username(), password.get_password(), password.get_url(), password.get_notes()));
        // }
        // std::fs::write(Self::USER_PASSWORDS_DIR, passwords_file_content).expect("Unable to write file");
        // // Encrypt the passwords file
        // // let mut salt = self.get_salt();
        // encrypt(Self::USER_PASSWORDS_DIR, &self.master_password).expect("Unable to encrypt file");
        // encrypt_file("Resource/user_passwords", "Resource/user_passwords.encrypted", &self.master_password, &mut salt);
        // Update the user DB
        println!("Logged out successfully!")
    }

    pub fn gen_password(&mut self, length: usize) -> String {
        //! Generate a random password
        //! # Arguments
        //! * `length` - The length of the password (usize)
        //! # Returns
        //! * `String` - The generated password

        let mut rng = rand::thread_rng();
        let mut password = String::new();
        for _ in 0..length {
            let random_char = rng.gen_range(33..=126) as u8;
            password.push(char::from(random_char));
        }
        password
    }
}

// fn encrypt(file_path: &str, pass:&str) -> Result<(), Box<dyn std::error::Error>> {
//     println!("Encrypting file...");
//     // Add .encrypted to the file path
//     let mut file_path = file_path.to_string();
//     let mut out_file_path = file_path.clone();
//     out_file_path.push_str(".encrypted");
//     let mut c = ef::Config::new();
//     c.input_stream(ef::InputStream::File(file_path.to_string()))
//         .output_stream(ef::OutputStream::File(out_file_path.to_string()))
//         .add_output_option(ef::OutputOption::AllowOverwrite)
//         .initialization_vector(ef::InitializationVector::GenerateFromRng)
//         .password(ef::PasswordType::Text(pass.to_string(),ef::scrypt_defaults()))
//         .encrypt();
//     let _ = ef::process(&c).map_err(|e| {
//         println!("Error: {:?}", e);
//         e
//     });
//     // Delete the original file
//     fs::remove_file(file_path).expect("Unable to delete file");
//     Ok(())
// }
//
// fn decrypt(file_path: &str, pass:&str) -> Result<(), Box<dyn std::error::Error>> {
//     println!("Decrypting file: {}", file_path);
//     // Remove .encrypted from the file path
//     let mut file_path = file_path.to_string();
//     let mut out_file_path = file_path.clone();
//     out_file_path = out_file_path.replace(".encrypted", "");
//
//     let mut c = ef::Config::new();
//     c.input_stream(ef::InputStream::File(file_path.to_string()))
//         .output_stream(ef::OutputStream::File(out_file_path.to_string()))
//         .add_output_option(ef::OutputOption::AllowOverwrite)
//         .password(ef::PasswordType::Text(pass.to_string(),ef::scrypt_defaults()))
//         .decrypt();
//     let _ = ef::process(&c).map_err(|e| {
//         println!("Error: {:?}", e);
//         e
//     });
//
//     // Delete the original file
//     fs::remove_file(file_path).expect("Unable to delete file");
//     Ok(())
// }