use std::error::Error;
use rusqlite::{Connection, params, Result};
use crate::config;
use crate::password::Password;
use crate::password_manager::PasswordManager;


pub(crate) struct DB { }

impl DB{
    pub fn pass_table_creation(username: &str) -> Result<(), Box<dyn Error>> {
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path)?;
        // SQL statement for creating passwords table with dynamic table name
        let sql = format!(
            "CREATE TABLE IF NOT EXISTS passwords_{} (
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            username TEXT NOT NULL,
            password TEXT NOT NULL,
            url TEXT NOT NULL,
            notes TEXT NOT NULL,
            user_id INTEGER NOT NULL
        );",
            username
        );
        conn.execute(&sql, params![])?;
        conn.close().expect("Could not close connection!");
        // println!("Password table for {} created successfully!", username);
        Ok(())
    }


    pub fn user_table_creation() -> Result<(), Box<dyn Error>> {
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path)?;
        conn.execute("
        CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY,
        username TEXT NOT NULL,
        password TEXT NOT NULL,
        salt TEXT NOT NULL
    )",
                     (),
        )?;
        conn.close().expect("Could not close connection!");
        Ok(())

    }

    pub fn insert_password(username:&str, password: Password) -> Result<(), Box<dyn Error>> {
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path)?;
        conn.execute(
            "INSERT INTO passwords_?1 (name, username, password, url, notes) VALUES (?2, ?3, ?4, ?5, ?6)",
            params![
            username,
            password.get_name(),
            password.get_username(),
            password.get_password(),
            password.get_url(),
            password.get_notes(),
        ],
        )?;
        conn.close().expect("Could not close connection!");
        println!("Password inserted successfully!");
        Ok(())
    }

    pub fn update_password(username: &str, password: Password) -> Result<(), Box<dyn Error>> {
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path)?;
        conn.execute(
            "UPDATE passwords_?1 SET name = ?2, username = ?3, password = ?4, url = ?5, notes = ?6 WHERE id = ?7",
            params![
            username,
            password.get_name(),
            password.get_username(),
            password.get_password(),
            password.get_url(),
            password.get_notes(),
            password.get_id(),
        ],
        )?;
        conn.close().expect("Could not close connection!");
        println!("Password updated successfully!");
        Ok(())
    }

    pub fn get_salt(username: &str) -> Result<String, Box<dyn Error>> {
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path)?;
        let mut stmt = conn.prepare("SELECT * FROM users WHERE username = ?1")?;
        let mut rows = stmt.query(&[&username])?;
        if let Some(row) = rows.next()? {
            let salt: String = row.get(3)?;
            Ok(salt)
        } else {
            Ok(String::from(""))
        }
    }

    pub fn auth_user(password_manager: &mut PasswordManager) -> Result<bool, Box<dyn Error>> {
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path)?;
        let mut stmt = conn.prepare("SELECT * FROM users WHERE username = ?1 AND password = ?2")?;
        let mut rows = stmt.query(&[&password_manager.get_user_name(), &password_manager.get_master_password()])?;
        if let Some(_) = rows.next()? {
            Ok(true)
        } else {
            Ok(false)
        }
    }

    pub fn get_user_id(password_manager: &mut PasswordManager) -> Result<usize, Box<dyn Error>> {
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path)?;
        let mut stmt = conn.prepare("SELECT * FROM users WHERE username = ?1 AND password = ?2")?;
        let mut rows = stmt.query(&[&password_manager.get_user_name(), &password_manager.get_master_password()])?;
        if let Some(row) = rows.next()? {
            let id: i32 = row.get(0)?;
            Ok(id as usize)
        }
        else {
            Err(Box::new(rusqlite::Error::QueryReturnedNoRows))
        }
    }

    pub fn get_max_user_id() -> Result<usize, Box<dyn Error>> {
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path)?;

        // Check if table is empty
        let mut stmt = conn.prepare("SELECT COUNT(*) FROM users")?;
        let count: i32 = stmt.query_row([], |row| row.get(0))?;
        if count == 0 {
            return Ok(0);
        }

        // Query for the maximum ID
        let mut stmt = conn.prepare("SELECT MAX(id) FROM users")?;
        let mut rows = stmt.query([])?;
        let mut max_id: usize = 0;
        while let Some(row) = rows.next()? {
            let id: i32 = row.get(0)?;
            max_id = id as usize;
        }

        Ok(max_id + 1)
    }


    pub fn get_all_password(password_manager: &PasswordManager) -> Result<Vec<Password>, Box<dyn Error>> {
        let mut passwords: Vec<Password> = Vec::new();
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path)?;
        let username = password_manager.get_user_name();

        let stmt = format!("SELECT * FROM passwords_{} WHERE user_id = ?1", username);
        let mut stmt = conn.prepare(&stmt)?;
        let mut rows = stmt.query(&[&password_manager.get_user_id()])?;
        while let Some(row) = rows.next()? {
            let id: usize = row.get(0)?;
            let name: String = row.get(1)?;
            let username: String = row.get(2)?;
            let password: String = row.get(3)?;
            let url: String = row.get(4)?;
            let notes: String = row.get(5)?;
            let password = Password::new(id, name, username, password, url, notes);
            passwords.push(password);
        }
        Ok(passwords)
    }

    pub fn create_user(password_manager: &mut PasswordManager) -> Result<(), Box<dyn Error>> {
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path)?;

        conn.execute(
            "INSERT INTO users (username, password, salt) VALUES (?1, ?2, ?3)",
            &[&password_manager.get_user_name(), &password_manager.get_master_password(), &password_manager.get_salt()],
        )?;
        Ok(())
    }

    pub fn remove_password(password_manager: &PasswordManager, id: usize) {
        let username = password_manager.get_user_name();
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path).expect("Could not open database!");
        conn.execute(
            format!("DELETE FROM passwords_{} WHERE id = ?1", username).as_str(),
            params![id],
        ).expect("Could not delete password!");
        conn.close().expect("Could not close connection!");
        println!("Password deleted successfully!");
    }

    pub fn get_max_password_id(password_manager: &PasswordManager) -> std::result::Result<usize, Box<dyn Error>> {
        let username = password_manager.get_user_name();
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path).expect("Could not open database!");
        let table_name = format!("passwords_{}", username);
        // Check if table is empty
        let mut stmt = conn.prepare(&format!("SELECT COUNT(*) FROM {}", table_name))?;
        let count: i32 = stmt.query_row([], |row| row.get(0))?;
        if count == 0 {
            return Ok(0);
        }
        // Query for the maximum ID (if table is not empty)
        let sql_query = format!("SELECT MAX(id) FROM {}", table_name);
        let mut stmt = conn.prepare(&sql_query)?;
        let mut rows = stmt.query([])?;
        let max_id: Option<i64> = rows.next()?.map(|row| row.get(0)).transpose()?;
        if let Some(id) = max_id {
            Ok(id as usize + 1)
        } else {
            Ok(0)
        }
    }



    pub fn add_password(password: Password, password_manager: &PasswordManager) -> Result<(), Box<dyn Error>> {
        let db_path = config::Config::get_db_path();
        let conn = Connection::open(db_path).expect("Could not open database!");
        let username = password_manager.get_user_name();
        let table_name = format!("passwords_{}", username);
        conn.execute(
            &format!("INSERT INTO {} (id, name, username, password, url, notes, user_id) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",table_name),
            params![
            password.get_id(),
            password.get_name(),
            password.get_username(),
            password.get_password(),
            password.get_url(),
            password.get_notes(),
            password_manager.get_user_id(),
        ],
        )?;
        conn.close().expect("Could not close connection!");
        println!("Password inserted successfully!");
        Ok(())
    }
}
