use crate::password_manager::PasswordManager;
use std::io;

pub fn add_password(password_manager: &mut PasswordManager) {
    //! Add a password to the password manager
    //! # Arguments
    //! * `password_manager` - The password manager object
    //! # Returns
    //! * `()` - Nothing

    println!("Enter the name of the entry: ");
    let mut name = String::new();
    io::stdin().read_line(&mut name).expect("Failed to read line");
    name = name.trim().to_string();

    println!("Enter the username: ");
    let mut username = String::new();
    io::stdin().read_line(&mut username).expect("Failed to read line");
    username = username.trim().to_string();

    println!("Enter the password: ");
    let mut password = String::new();
    io::stdin().read_line(&mut password).expect("Failed to read line");
    password = password.trim().to_string();

    println!("Enter the url: ");
    let mut url = String::new();
    io::stdin().read_line(&mut url).expect("Failed to read line");
    url = url.trim().to_string();

    println!("Enter the notes: ");
    let mut notes = String::new();
    io::stdin().read_line(&mut notes).expect("Failed to read line");
    notes = notes.trim().to_string();

    password_manager.add_password(name, username, password, url, notes);
}

pub fn remove_password(password_manager: &mut PasswordManager) {
    // Print all entries
    // Ask for the name of the entry to be removed
    if password_manager.get_passwords().unwrap().is_empty() {
        println!("No passwords found!");
        return;
    }

    get_all_password(&password_manager);
    println!("Enter the index of the entry: ");
    let mut index = String::new();
    io::stdin().read_line(&mut index).expect("Failed to read line");
    let num:i32 = match index.trim().parse() {
            Ok(num) if num >= 0 => num,
            _ => {
                println!("Invalid choice!");
                return;
            }
    };
    // Remove the entry
    password_manager.remove_password_from_index(num as usize);
}

pub fn get_password(password_manager: &mut PasswordManager) {
    if password_manager.get_passwords().unwrap().is_empty() {
        println!("No passwords found!");
        return;
    }

    println!("Enter the name of the entry: ");
    let mut name = String::new();
    io::stdin().read_line(&mut name).expect("Failed to read line");
    name = name.trim().to_string();

    let received = {
        match password_manager.clone().get_password(&name) {
            Ok(password) => password,
            Err(_) => {
                println!("No password found with that name!");
                return;
            }
        }
    };
    println!("========================================");
    println!("Name: {}\nUsername: {}\nPassword: {}\nUrl: {}\nNotes: {}", received.get_name(), received.get_username(), received.get_password(), received.get_url(), received.get_notes());
    println!("========================================");
}

pub fn get_all_password(password_manager: &PasswordManager) {
    let passwords = {
        match password_manager.get_passwords() {
            Ok(passwords) => {
                if passwords.is_empty() || passwords.len() == 0 {
                    println!("No passwords found!");
                    return;
                }
                passwords
            }
            Err(_) => {
                println!("No passwords found!");
                return;
            }
        }
    };
    println!("========================================");
    for password in passwords {

        println!("----------------------------------------");
        println!("Password ID: {}", password.get_id());
        println!("----------------------------------------");
        println!("Name: {}\nUsername: {}\nPassword: {}\nUrl: {}\nNotes: {}", password.get_name(), password.get_username(), password.get_password(), password.get_url(), password.get_notes());
        println!("----------------------------------------");
    }
    println!("========================================");
}

pub fn gen_password(password_manager: &mut PasswordManager){
    println!("Enter the length of the password: ");
    let mut length = String::new();
    io::stdin().read_line(&mut length).expect("Failed to read line");
    let length:u32 = {
        match length.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Invalid choice!");
                return;
            }
        }
    };
    // let password = password_manager.gen_password(length as usize);
    println!("Generated password: {}", password_manager.gen_password(length as usize));
}
